import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Button } from 'antd';

const WrapperButton = styled(Button)`
 &, &.ant-btn
 {
    border-radius: 1000px;
    &.marginRight {
        margin-right: 20px;
    }
    &.gradient {
        border: none;
        height: 48px;
        font-size: 16px;
        padding: 8px 24px;
        transition: .3s cubic-bezier(.19, 1, .22, 1);

        &.ant-btn-primary {
            background: linear-gradient(to right , #f00050, #ff7350);
            &:hover,
            &:focus {
                background: linear-gradient(to right , #f00050, #ff7350);
                box-shadow: 0 0 7px #f00050;
            }

            &[disabled] {
                cursor: not-allowed;
                color: #ffffff;
                opacity: 0.7;
                * {
                    pointer-events: none;
                }
            }
        }
        &.ant-btn-secondary {
            height: 48px;
        }
    }
    &.ant-btn-secondary {
        background: linear-gradient(to right , #f00050, #ff7350);
        height: auto;
        font-size: 16px;
        padding: 8px 24px;
        transition: .3s cubic-bezier(.19, 1, .22, 1);
        &:after {
            content: '';
            position: absolute;
            top: 1px;
            right: 1px;
            bottom: 1px;
            left: 1px;
            background: #fff;
            border-radius: 1000px;
        }
        >span {
            position: relative;
            z-index: 1;
            color: #f00050;
            line-height: 16px;
        }
    }
}
`;

const RoundButton = (props) => {
  const additionalClasses = [];
  if (props['data-gradient']) {
    additionalClasses.push('gradient');
  }
  return (
    <WrapperButton className={`RoundButton ${[...additionalClasses]}`} {...props}>
      {props.label}
    </WrapperButton>
  );
};

RoundButton.propTypes = {
  label: PropTypes.string.isRequired,
  'data-gradient': PropTypes.bool,
};

RoundButton.defaultProps = {
  'data-gradient': false,
};

export default RoundButton;
