'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _button = require('antd/lib/button');

var _button2 = _interopRequireDefault(_button);

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _templateObject = _taggedTemplateLiteral(['\n &, &.ant-btn\n {\n    border-radius: 1000px;\n    &.marginRight {\n        margin-right: 20px;\n    }\n    &.gradient {\n        border: none;\n        height: 48px;\n        font-size: 16px;\n        padding: 8px 24px;\n        transition: .3s cubic-bezier(.19, 1, .22, 1);\n\n        &.ant-btn-primary {\n            background: linear-gradient(to right , #f00050, #ff7350);\n            &:hover,\n            &:focus {\n                background: linear-gradient(to right , #f00050, #ff7350);\n                box-shadow: 0 0 7px #f00050;\n            }\n\n            &[disabled] {\n                cursor: not-allowed;\n                color: #ffffff;\n                opacity: 0.7;\n                * {\n                    pointer-events: none;\n                }\n            }\n        }\n        &.ant-btn-secondary {\n            height: 48px;\n        }\n    }\n    &.ant-btn-secondary {\n        background: linear-gradient(to right , #f00050, #ff7350);\n        height: auto;\n        font-size: 16px;\n        padding: 8px 24px;\n        transition: .3s cubic-bezier(.19, 1, .22, 1);\n        &:after {\n            content: \'\';\n            position: absolute;\n            top: 1px;\n            right: 1px;\n            bottom: 1px;\n            left: 1px;\n            background: #fff;\n            border-radius: 1000px;\n        }\n        >span {\n            position: relative;\n            z-index: 1;\n            color: #f00050;\n            line-height: 16px;\n        }\n    }\n}\n'], ['\n &, &.ant-btn\n {\n    border-radius: 1000px;\n    &.marginRight {\n        margin-right: 20px;\n    }\n    &.gradient {\n        border: none;\n        height: 48px;\n        font-size: 16px;\n        padding: 8px 24px;\n        transition: .3s cubic-bezier(.19, 1, .22, 1);\n\n        &.ant-btn-primary {\n            background: linear-gradient(to right , #f00050, #ff7350);\n            &:hover,\n            &:focus {\n                background: linear-gradient(to right , #f00050, #ff7350);\n                box-shadow: 0 0 7px #f00050;\n            }\n\n            &[disabled] {\n                cursor: not-allowed;\n                color: #ffffff;\n                opacity: 0.7;\n                * {\n                    pointer-events: none;\n                }\n            }\n        }\n        &.ant-btn-secondary {\n            height: 48px;\n        }\n    }\n    &.ant-btn-secondary {\n        background: linear-gradient(to right , #f00050, #ff7350);\n        height: auto;\n        font-size: 16px;\n        padding: 8px 24px;\n        transition: .3s cubic-bezier(.19, 1, .22, 1);\n        &:after {\n            content: \'\';\n            position: absolute;\n            top: 1px;\n            right: 1px;\n            bottom: 1px;\n            left: 1px;\n            background: #fff;\n            border-radius: 1000px;\n        }\n        >span {\n            position: relative;\n            z-index: 1;\n            color: #f00050;\n            line-height: 16px;\n        }\n    }\n}\n']);

require('antd/lib/button/style/css');

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var WrapperButton = (0, _styledComponents2.default)(_button2.default)(_templateObject);

var RoundButton = function RoundButton(props) {
    var additionalClasses = [];
    if (props['data-gradient']) {
        additionalClasses.push('gradient');
    }
    return _react2.default.createElement(
        WrapperButton,
        _extends({ className: 'RoundButton ' + [].concat(additionalClasses) }, props),
        props.label
    );
};

RoundButton.propTypes = {
    label: _propTypes2.default.string.isRequired,
    'data-gradient': _propTypes2.default.bool
};

RoundButton.defaultProps = {
    'data-gradient': false
};

exports.default = RoundButton;